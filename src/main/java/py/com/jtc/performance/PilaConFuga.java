package py.com.jtc.performance;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class PilaConFuga<E> implements Iterable<E> {

	private int N;
	private E[] array;

	@SuppressWarnings("unchecked")
	public PilaConFuga(int capacity) {
		array = (E[]) new Object[capacity];
	}

	public Iterator<E> iterator() {
		return new IteradorPila();
	}

	private class IteradorPila implements Iterator<E> {

		private int i = N - 1;

		public boolean hasNext() {
			return i >= 0;
		}

		public E next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}
			return array[i--];
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}
	}

	public void push(E item) {
		if (isFull()) {
			throw new RuntimeException("Stack overflow");
		}
		array[N++] = item;
	}

	public E pop() {
		if (isEmpty())
			throw new RuntimeException("Stack underflow");
		E item = array[--N];
		//array[N] = null; //si hacemos que se haga null el GC podra ir elminando estos objetos
		return item;
	}

	public boolean isEmpty() {
		return N == 0;
	}

	public int size() {
		return N;
	}

	public boolean isFull() {
		return N == array.length;
	}

	public E peek() {
		if (isEmpty())
			throw new RuntimeException("Stack underflow");
		return array[N - 1];
	}

	public static void main(String[] args) {
		
		PilaConFuga<Integer> s = new PilaConFuga<>(1000000);
		for (long l = 0; l < Long.MAX_VALUE; l++) {
			for (int i = 0; i < 1000000; i++) {
				s.push(i);
			}
			while (!s.isEmpty()) {
				s.pop();
			}
		}
		
		while (true) {
			// hacer algo aqui..
		}
	}
}
